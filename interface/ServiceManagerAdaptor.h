#ifndef _MANAGER_ADAPTOR_H_
#define _MANAGER_ADAPTOR_H_

#include <string>
#include <memory>
#include <glib.h>
#include <map>
#include <set>

#include "GDBusServiceManager.h"
#include "DBusAdaptor.hpp"

class ServiceManagerAdaptor : public DBusAdaptor<ComOsLeafServiceManager, ServiceManagerAdaptor>
{
public:
    ServiceManagerAdaptor();
    virtual ~ServiceManagerAdaptor();

    std::string GetServiceName() const override
    {
        return "com.os.leaf.service.manager";
    }

    std::string GetObjectPath() const override
    {
        return "/com/os/leaf/service/manager";
    }

    ComOsLeafServiceManager* AdaptorCreateSkeleton() const override
    {
        return com_os_leaf_service_manager_skeleton_new();
    }

private:
    ServiceManagerAdaptor(const ServiceManagerAdaptor&) = delete;
    ServiceManagerAdaptor& operator=(const ServiceManagerAdaptor&) = delete;
    
};

#endif