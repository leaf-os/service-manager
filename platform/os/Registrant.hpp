/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Registrant.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_REGISTRANT_HPP_
#define ANALYZER_REGISTRANT_HPP_

#include <exception>
#include <memory>
#include <string>

#include <AsyncResult.hpp>
#include <Handler.hpp>
#include <Message.hpp>

namespace android
{

    class Registrant
    {
    public:
        Registrant(const std::shared_ptr<Handler> &h, int32_t w, const std::shared_ptr<void> &obj);
        virtual ~Registrant();

        void clear();
        void notifyRegistrant();
        void notifyResult(const std::shared_ptr<void> &result);
        void notifyException(const std::shared_ptr<std::exception> &ex);
        void notifyRegistrant(const std::shared_ptr<AsyncResult> &ar);
        std::shared_ptr<Message> messageForRegistrant();
        std::shared_ptr<Handler> getHandler();
        void internalNotifyRegistrant(const std::shared_ptr<void> &result, const std::shared_ptr<std::exception> &ex);

    private:
        std::weak_ptr<Handler> refH;
        int32_t what;
        std::shared_ptr<void> userObj;
    };

} // namespace android

#endif // ANALYZER_REGISTRANT_HPP_
