/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_CONDITION_VARIABLE_HPP_
#define ANALYZER_CONDITION_VARIABLE_HPP_

#include <cassert>
#include <chrono>
#include <mutex>

#include <pthread.h>
#include <time.h>

namespace android
{

    enum class cv_status
    {
        no_timeout,
        timeout
    };

    class ConditionVariable
    {
        using steady_clock = std::chrono::steady_clock;

      public:
        ConditionVariable()
        {
            pthread_condattr_t attr;
            pthread_condattr_init(&attr);
            pthread_condattr_setclock(&attr, CLOCK_MONOTONIC);

            pthread_cond_init(&mCond, &attr);
            pthread_condattr_destroy(&attr);
        }

        virtual ~ConditionVariable()
        {
            pthread_cond_destroy(&mCond);
        }

        void notify_one() noexcept
        {
            int32_t err = pthread_cond_signal(&mCond);
            assert(err == 0);
        }

        void notify_all() noexcept
        {
            int32_t err = pthread_cond_broadcast(&mCond);
            assert(err == 0);
        }

        void wait(std::unique_lock<std::mutex> &lock)
        {
            int32_t err = pthread_cond_wait(&mCond, lock.mutex()->native_handle());
            assert(err == 0);
        }

        template <typename Predicate>
        void wait(std::unique_lock<std::mutex> &lock, Predicate stop_waiting)
        {
            while (!stop_waiting())
            {
                wait(lock);
            }
        }

        // reltime : nanoseconds
        // int32_t wait_for(std::unique_lock<std::mutex> &lock, int64_t reltime)
        // {
        //     struct timespec ts;
        //     clock_gettime(CLOCK_MONOTONIC, &ts);

        //     int64_t reltime_sec = reltime / 1000;

        //     ts.tv_nsec += static_cast<long>(reltime % 1000) * 1000000;
        //     if (reltime_sec < std::numeric_limits<int64_t>::max() && ts.tv_nsec >= 1000000000)
        //     {
        //         ts.tv_nsec -= 1000000000;
        //         ++reltime_sec;
        //     }

        //     int64_t time_sec = ts.tv_sec;
        //     if (time_sec > std::numeric_limits<int64_t>::max() - reltime_sec)
        //     {
        //         time_sec = std::numeric_limits<int64_t>::max();
        //     }
        //     else
        //     {
        //         time_sec += reltime_sec;
        //     }

        //     ts.tv_sec = (time_sec > std::numeric_limits<long>::max()) ? std::numeric_limits<long>::max()
        //                                                               : static_cast<long>(time_sec);

        //     return -pthread_cond_timedwait(&mCond, lock.mutex()->native_handle(), &ts);
        // }

        template <typename Duration>
        cv_status wait_until(std::unique_lock<std::mutex> &lock,
                             const std::chrono::time_point<steady_clock, Duration> &timeout_time)
        {
            return wait_until_impl(lock, timeout_time);
        }

        template <typename Duration, typename Predicate>
        bool wait_until(std::unique_lock<std::mutex> &lock,
                        const std::chrono::time_point<steady_clock, Duration> &timeout_time, Predicate stop_waiting)
        {
            while (!stop_waiting())
            {
                if (wait_until(lock, timeout_time) == cv_status::timeout)
                {
                    return stop_waiting();
                }
            }
            return true;
        }

        template <typename Rep, typename Period>
        cv_status wait_for(std::unique_lock<std::mutex> &lock, const std::chrono::duration<Rep, Period> &rel_time)
        {
            return wait_until(lock, steady_clock::now() + rel_time);
        }

        template <typename Rep, typename Period, typename Predicate>
        bool wait_for(std::unique_lock<std::mutex> &lock, const std::chrono::duration<Rep, Period> &rel_time,
                      Predicate stop_waiting)
        {
            return wait_until(lock, steady_clock::now() + rel_time, std::move(stop_waiting));
        }

      private:
        template <typename Duratrion>
        cv_status wait_until_impl(std::unique_lock<std::mutex> &lock,
                                  const std::chrono::time_point<steady_clock, Duratrion> &timeout_time)
        {
            auto sec = std::chrono::time_point_cast<std::chrono::seconds>(timeout_time);
            auto nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(timeout_time - sec);

            struct timespec ts = {
                static_cast<std::time_t>(sec.time_since_epoch().count()), //
                static_cast<long>(nsec.count())                           //
            };

            pthread_cond_timedwait(&mCond, lock.mutex()->native_handle(), &ts);

            return (steady_clock::now() < timeout_time ? cv_status::no_timeout : cv_status::timeout);
        }

      private:
        pthread_cond_t mCond;
    };

} // namespace android

#endif // ANALYZER_CONDITION_VARIABLE_HPP_