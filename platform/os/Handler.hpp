/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/Handler.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_HANDLER_HPP_
#define ANALYZER_HANDLER_HPP_

#include <memory>
#include <string>

namespace android
{

    class Looper;
    class Message;
    class MessageQueue;

    class Handler : public std::enable_shared_from_this<Handler>
    {
    public:
        Handler();
        explicit Handler(const std::shared_ptr<Looper> &looper);
        virtual ~Handler() = default;

        virtual void handleMessage(const std::shared_ptr<Message> &msg);

        void dispatchMessage(const std::shared_ptr<Message> &msg);

        std::shared_ptr<Message> obtainMessage();
        std::shared_ptr<Message> obtainMessage(int32_t what);
        std::shared_ptr<Message> obtainMessage(int32_t what, int32_t arg1);
        std::shared_ptr<Message> obtainMessage(int32_t what, int32_t arg1, int32_t arg2);
        std::shared_ptr<Message> obtainMessage(int32_t what, int32_t arg1, int32_t arg2, int32_t arg3);
        std::shared_ptr<Message> obtainMessage(int32_t what, const std::shared_ptr<void> &obj);
        std::shared_ptr<Message> obtainMessage(int32_t what, int32_t arg1, const std::shared_ptr<void> &obj);
        std::shared_ptr<Message> obtainMessage(int32_t what, int32_t arg1, int32_t arg2, const std::shared_ptr<void> &obj);
        std::shared_ptr<Message> obtainMessage(int32_t what, int32_t arg1, int32_t arg2, int32_t arg3, const std::shared_ptr<void> &obj);

        bool sendMessage(const std::shared_ptr<Message> &msg);
        bool sendEmptyMessage(int32_t what);
        bool sendEmptyMessageDelayed(int32_t what, uint64_t delayMillis);
        bool sendEmptyMessageAtTime(int32_t what, uint64_t uptimeMillis);
        bool sendMessageDelayed(const std::shared_ptr<Message> &msg, uint64_t delayMillis);
        bool sendMessageAtTime(const std::shared_ptr<Message> &msg, uint64_t uptimeMillis);
        bool sendMessageAtFrontOfQueue(const std::shared_ptr<Message> &msg);

        void removeMessages(int32_t what);
        void removeMessages(int32_t what, const std::shared_ptr<void> &obj);
        void removeMessages(int32_t what, int32_t arg1);

        bool hasMessages(int32_t what);
        bool hasMessages(int32_t what, const std::shared_ptr<void> &obj);

        std::shared_ptr<Looper> getLooper() const;

#ifdef __DEBUG__
        void setDebugName(const std::string &name);
        std::string getDebugName() const;
#endif

    private:
        Handler(const Handler &) = delete;
        Handler &operator=(const Handler &) = delete;

        bool enqueueMessage(const std::shared_ptr<MessageQueue> &queue, const std::shared_ptr<Message> &msg, uint64_t uptimeMillis);

    private:
        std::shared_ptr<Looper> mLooper;
        std::shared_ptr<MessageQueue> mQueue;

#ifdef __DEBUG__
        std::string mDebugName;
#endif
    };

#ifdef __DEBUG__
#define SET_HANDLER_NAME(name) setDebugName(#name)
#define SET_MESSAGE_NAME(msg, name) (msg)->setDebugName(#name)
#endif

} // namespace android

#endif // ANALYZER_HANDLER_HPP_
