/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_MESSAGE_QUEUE_HPP_
#define ANALYZER_MESSAGE_QUEUE_HPP_

#include <memory>

#include <ConditionVariable.hpp>
#include <mutex>

namespace android
{

    class Message;
    class Handler;

    /**
     * Low-level class holding the list of messages to be dispatched by a
     * {@link Looper}.  Messages are not added directly to a MessageQueue,
     * but rather through {@link Handler} objects associated with the Looper.
     *
     * <p>You can retrieve the MessageQueue for the current thread with
     * {@link Looper#myQueue() Looper::myQueue()}.
     */
#ifndef __UNITTEST__

    class MessageQueue final
    {
    public:
        explicit MessageQueue(bool quitAllowed);
        virtual ~MessageQueue() = default;
#else
    class MessageQueue
    {
    public:
        explicit MessageQueue(bool quitAllowed);
        virtual ~MessageQueue();
#endif
        bool isIdle();
        std::shared_ptr<Message> next();
        void quit(bool safe);
        bool enqueueMessage(const std::shared_ptr<Message> &msg, uint64_t when);
        bool hasMessages(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj);
        void removeMessages(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj);
        void removeMessages(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1);

    private:
        MessageQueue(const MessageQueue &) = delete;
        MessageQueue &operator=(const MessageQueue &) = delete;

        void removeAllMessagesLocked();
        void removeAllFutureMessagesLocked();

#ifdef __DEBUG__
        void dump();
#endif

    private:
        std::shared_ptr<Message> mMessages;
        bool mQuitAllowed;
        bool mQuitting;

#ifdef __DEBUG__
        int32_t mCount;
#endif

        ConditionVariable mCondition;
        std::mutex mMutex;
    };

} // namespace android

#endif // ANALYZER_MESSAGE_QUEUE_HPP_
