/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/RegistrantList.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANALYZER_REGISTRANT_LIST_HPP_
#define ANALYZER_REGISTRANT_LIST_HPP_

#include <exception>
#include <memory>
#include <mutex>
#include <vector>

#include <AsyncResult.hpp>
#include <Handler.hpp>
#include <Registrant.hpp>

namespace android
{

    class RegistrantList
    {
    public:
        RegistrantList();
        virtual ~RegistrantList();

        void add(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj);
        void addUnique(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj);
        void add(const std::shared_ptr<Registrant> &r);
        void removeCleared();
        uint64_t size();

        void notifyRegistrants();
        void notifyException(const std::shared_ptr<std::exception> &ex);
        void notifyResult(const std::shared_ptr<void> &result);
        void notifyRegistrants(const std::shared_ptr<AsyncResult> &ar);
        void remove(const std::shared_ptr<Handler> &h);

    private:
        void internalNotifyRegistrants(const std::shared_ptr<void> &result, const std::shared_ptr<std::exception> &ex);

    private:
        std::vector<std::shared_ptr<Registrant>> mRegistrants;
        std::recursive_mutex mMutex;
    };

} // namespace android

#endif // ANALYZER_REGISTRANT_LIST_HPP_
