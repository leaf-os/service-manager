/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/frameworks/base/core/java/android/os/MessageQueue.java
 */
/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <MessageQueue.hpp>

#include <algorithm>
#include <chrono>
#include <limits>

#include <Message.hpp>
#include <SystemClock.hpp>
#include <Tlog.hpp>

namespace android
{

    namespace
    {

        constexpr const char *const kLogTag = "MessageQueue";

        constexpr int32_t kWarningProcessTimeMS = 1000;
        constexpr int32_t kWarningMessageCount = 30;

    } // namespace

    MessageQueue::MessageQueue(bool quitAllowed) : mQuitAllowed(quitAllowed), mQuitting(false)
    {
#ifdef __DEBUG__
        mCount = 0;
#endif
    }

    bool MessageQueue::isIdle()
    {
#if defined(__ANDROID_CONDITION__)
        android::Mutex::Autolock lock(mMutex);
#else
        std::lock_guard<std::mutex> lock(mMutex);
#endif

        const uint64_t now = SystemClock::uptimeMillis();
        return mMessages == nullptr || now < mMessages->when;
    }

    std::shared_ptr<Message> MessageQueue::next()
    {
        for (;;)
        {
#if defined(__ANDROID_CONDITION__)
            android::Mutex::Autolock lock(mMutex);
#else
            std::unique_lock<std::mutex> lock(mMutex);
#endif

            if (mQuitting)
            {
                return nullptr;
            }

            const uint64_t now = SystemClock::uptimeMillis();
            auto msg = mMessages;

            if (msg != nullptr)
            {
                if (now < msg->when)
                {
                    auto timeoutMillis = std::min(msg->when - now, static_cast<uint64_t>(std::numeric_limits<int32_t>::max()));

#if defined(__ANDROID_CONDITION__)
                    mCondition.waitRelative(mMutex, ms2ns(timeoutMillis));
#else
                    mCondition.wait_for(lock, std::chrono::milliseconds(timeoutMillis));
#endif
                }
                else
                {
                    mMessages = msg->next;
                    msg->next = nullptr;

#ifdef __DEBUG__
                    if (now - msg->when > kWarningProcessTimeMS)
                    {
                        //TLOGV(kLogTag, "*** %d ms : %s", now - msg->when, msg->toString().c_str());
                    }

                    mCount--;
                    if (mCount > kWarningMessageCount)
                    {
                        dump();
                    }
#endif

                    msg->markInUse();
                    return msg;
                }
            }
            else
            {
#if defined(__ANDROID_CONDITION__)
                mCondition.wait(mMutex);
#else
                mCondition.wait(lock);
#endif
            }
        }
    }

    void MessageQueue::quit(bool safe)
    {
        if (!mQuitAllowed)
        {
            //TLOGW(kLogTag, "Main thread not allowed to quit.");
            return;
        }

#if defined(__ANDROID_CONDITION__)
        android::Mutex::Autolock lock(mMutex);
#else
        std::lock_guard<std::mutex> lock(mMutex);
#endif

        if (mQuitting)
        {
            return;
        }
        mQuitting = true;

        if (safe)
        {
            removeAllFutureMessagesLocked();
        }
        else
        {
            removeAllMessagesLocked();
        }

#if defined(__ANDROID_CONDITION__)
        mCondition.signal();
#else
        mCondition.notify_one();
#endif
    }

    bool MessageQueue::enqueueMessage(const std::shared_ptr<Message> &msg, uint64_t when)
    {
        if (msg->target == nullptr)
        {
            //TLOGW(kLogTag, "Message must have a target");
            return false;
        }

        if (msg->isInUse())
        {
            //TLOGW(kLogTag, "This message is already in use");
            return false;
        }

#if defined(__ANDROID_CONDITION__)
        android::Mutex::Autolock lock(mMutex);
#else
        std::lock_guard<std::mutex> lock(mMutex);
#endif

        if (mQuitting)
        {
            //TLOGW(kLogTag, "sending message to a Handler on a dead thread");
            msg->recycle();
            return false;
        }

        msg->markInUse();
        msg->when = when;

        auto p = mMessages;
        if (p == nullptr || when == 0 || when < p->when)
        {
            msg->next = p;
            mMessages = msg;
        }
        else
        {
            std::shared_ptr<Message> prev;
            for (;;)
            {
                prev = p;
                p = p->next;
                if (p == nullptr || when < p->when)
                {
                    break;
                }
            }
            msg->next = p;
            prev->next = msg;
        }

#ifdef __DEBUG__
        mCount++;
#endif

#if defined(__ANDROID_CONDITION__)
        mCondition.signal();
#else
        mCondition.notify_one();
#endif

        return true;
    }

    bool MessageQueue::hasMessages(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj)
    {
        if (h == nullptr)
        {
            return false;
        }

#if defined(__ANDROID_CONDITION__)
        android::Mutex::Autolock lock(mMutex);
#else
        std::lock_guard<std::mutex> lock(mMutex);
#endif

        auto p = mMessages;
        while (p != nullptr)
        {
            if (p->target == h && p->what == what && (obj == nullptr || p->obj == obj))
            {
                return true;
            }
            p = p->next;
        }
        return false;
    }

    void MessageQueue::removeMessages(const std::shared_ptr<Handler> &h, int32_t what, const std::shared_ptr<void> &obj)
    {
        if (h == nullptr)
        {
            return;
        }

#if defined(__ANDROID_CONDITION__)
        android::Mutex::Autolock lock(mMutex);
#else
        std::lock_guard<std::mutex> lock(mMutex);
#endif

        // Remove all messages at front
        auto p = mMessages;
        while (p != nullptr && p->target == h && p->what == what && (obj == nullptr || p->obj == obj))
        {
            auto n = p->next;
            mMessages = n;
            p->recycleUnchecked();
#ifdef __DEBUG__
            mCount--;
#endif
            p = n;
        }

        // Remove all messages after front
        while (p != nullptr)
        {
            auto n = p->next;
            if (n != nullptr)
            {
                if (n->target == h && n->what == what && (obj == nullptr || n->obj == obj))
                {
                    auto nn = n->next;
                    n->recycleUnchecked();
#ifdef __DEBUG__
                    mCount--;
#endif
                    p->next = nn;
                    continue;
                }
            }
            p = n;
        }
    }

    void MessageQueue::removeMessages(const std::shared_ptr<Handler> &h, int32_t what, int32_t arg1)
    {
        if (h == nullptr)
        {
            return;
        }

#if defined(__ANDROID_CONDITION__)
        android::Mutex::Autolock lock(mMutex);
#else
        std::lock_guard<std::mutex> lock(mMutex);
#endif

        // Remove all messages at front
        auto p = mMessages;
        while (p != nullptr && p->target == h && p->what == what && p->arg1 == arg1)
        {
            auto n = p->next;
            mMessages = n;
            p->recycleUnchecked();
#ifdef __DEBUG__
            mCount--;
#endif
            p = n;
        }

        // Remove all messages after front
        while (p != nullptr)
        {
            auto n = p->next;
            if (n != nullptr)
            {
                if (n->target == h && n->what == what && n->arg1 == arg1)
                {
                    auto nn = n->next;
                    n->recycleUnchecked();
#ifdef __DEBUG__
                    mCount--;
#endif
                    p->next = nn;
                    continue;
                }
            }
            p = n;
        }
    }

    void MessageQueue::removeAllMessagesLocked()
    {
        auto p = mMessages;
        while (p != nullptr)
        {
            auto n = p->next;
            p->recycleUnchecked();
#ifdef __DEBUG__
            mCount--;
#endif
            p = n;
        }
        mMessages = nullptr;
    }

    void MessageQueue::removeAllFutureMessagesLocked()
    {
        const uint64_t now = SystemClock::uptimeMillis();
        auto p = mMessages;
        if (p != nullptr)
        {
            if (p->when > now)
            {
                removeAllMessagesLocked();
            }
            else
            {
                std::shared_ptr<Message> n;
                for (;;)
                {
                    n = p->next;
                    if (n == nullptr)
                    {
                        return;
                    }
                    if (n->when > now)
                    {
                        break;
                    }
                    p = n;
                }
                p->next = nullptr;
                do
                {
                    p = n;
                    n = p->next;
                    p->recycleUnchecked();
#ifdef __DEBUG__
                    mCount--;
#endif
                } while (n != nullptr);
            }
        }
    }

#ifdef __DEBUG__
    void MessageQueue::dump()
    {
        const uint64_t now = SystemClock::uptimeMillis();

        //TLOGV(kLogTag, "*** No of messages : %d", mCount);
        //TLOGV(kLogTag, "***   Now: %llu", now);

        int32_t no = 0;
        auto p = mMessages;
        while (p != nullptr)
        {
            auto n = p->next;
            //TLOGV(kLogTag, "*** [%02d] %s", no, p->toString().c_str());
            no++;
            p = n;
        }
    }
#endif

} // namespace android
