/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/dalvik/dx/src/com/android/dx/util/ByteArray.java
 */
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DIAG_BYTE_ARRAY_HPP_
#define DIAG_BYTE_ARRAY_HPP_

#include <string>
#include <vector>

namespace android
{

    class ByteArray
    {
    public:
        ByteArray(const uint8_t *bytes, uint64_t size);
        explicit ByteArray(uint64_t size);
        explicit ByteArray(const ByteArray &byteArray);
        explicit ByteArray(const std::basic_string<uint8_t> &bytes);
        virtual ~ByteArray() = default;

        ByteArray &operator=(const ByteArray &other);
        const uint8_t &operator[](int32_t offset) const;
        uint8_t &operator[](int32_t offset);

        bool empty() const;
        uint64_t size() const;

        void putByte(int32_t offset, uint8_t value);

        uint32_t getByte(int32_t offset = 0) const;

        int32_t getInt8(int32_t offset = 0) const;
        int32_t getInt16(int32_t offset = 0) const;
        int32_t getInt32(int32_t offset = 0) const;
        int64_t getInt64(int32_t offset = 0) const;

        uint32_t getUint8(int32_t offset = 0) const;
        uint32_t getUint16(int32_t offset = 0) const;
        uint32_t getUint32(int32_t offset = 0) const;
        uint64_t getUint64(int32_t offset = 0) const;

        const uint8_t *getBytes(int32_t offset = 0) const;
        std::basic_string<uint8_t> getByteArray() const;
        std::vector<uint8_t> getVector() const;

        std::string getString(int32_t offset = 0) const;
        std::string getString(int32_t offset, uint64_t size) const;

        std::u16string getU16String(int32_t offset) const;
        std::u16string getU16String(int32_t offset, uint64_t size) const;

        std::string toString() const;

    private:
        void checkOffsets(int32_t start, int32_t end) const;
        uint32_t getByte0(int32_t offset) const;

    private:
        std::basic_string<uint8_t> mBytes;
    };

} // namespace android

#endif // DIAG_BYTE_ARRAY_HPP_
