/* This source code is converted from Android Open Source Project to port into telematics,
 * and this is header from original file,
 * android/system/core/include/log/log.h
 */
/*
 * Copyright (C) 2005-2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define DIAG_TLOG_HPP_
#ifndef DIAG_TLOG_HPP_
#define DIAG_TLOG_HPP_

#include <log/log.h>

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef TLOGV
#define TLOGV(TLOG_TAG, ...) \
    ((void)__android_log_buf_print(LOG_ID_RADIO, ANDROID_LOG_VERBOSE, TLOG_TAG, __VA_ARGS__))
#endif

#ifndef TLOGD
#define TLOGD(TLOG_TAG, ...) \
    ((void)__android_log_buf_print(LOG_ID_RADIO, ANDROID_LOG_DEBUG, TLOG_TAG, __VA_ARGS__))
#endif

#ifndef TLOGI
#define TLOGI(TLOG_TAG, ...)                                                            \
    do                                                                                  \
    {                                                                                   \
        __android_log_buf_print(LOG_ID_RADIO, ANDROID_LOG_INFO, TLOG_TAG, __VA_ARGS__); \
    } while (0)
#endif

#ifndef TLOGW
#define TLOGW(TLOG_TAG, ...)                                                            \
    do                                                                                  \
    {                                                                                   \
        __android_log_buf_print(LOG_ID_RADIO, ANDROID_LOG_WARN, TLOG_TAG, __VA_ARGS__); \
    } while (0)
#endif

#ifndef TLOGE
#define TLOGE(TLOG_TAG, ...)                                                             \
    do                                                                                   \
    {                                                                                    \
        __android_log_buf_print(LOG_ID_RADIO, ANDROID_LOG_ERROR, TLOG_TAG, __VA_ARGS__); \
    } while (0)
#endif

#if defined(RIL_SPECIAL_LOG)
#ifndef TLOG_RADIO
#define TLOG_RADIO(...) ((void)__android_log_buf_print(LOG_ID_SYSTEM, ANDROID_LOG_ERROR, "TELE", __VA_ARGS__))
#endif
#endif

#ifdef __cplusplus
}
#endif

#endif // DIAG_TLOG_HPP_
