#include <stdio.h>
#include <memory>
#include "LogService.h"
#include "ServiceManager.h"

int main()
{
    LogService::getInstance()->registerService(std::make_shared<Logger>());
    LOG_INFO("Init ServiceManager main");;

    ServiceManager *service = new ServiceManager();
    service->instantiate();
    service->onInit();
    service->onStart();

    LOG_ERROR("ServiceManager main stopped unexpectedly");
    return 1;
}