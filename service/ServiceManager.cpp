#include <glib.h>

#include "LogService.h"
#include "ServiceManager.h"
#include "Commander.hpp"

ServiceManager::ServiceManager()
{

}

ServiceManager::~ServiceManager()
{

}

bool ServiceManager::onInit()
{
    LOG_INFO("onInit");

    return true;
}

void ServiceManager::instantiate()
{
    LOG_INFO("instantiate");
}

error_t ServiceManager::onStart()
{
    LOG_INFO("Onstart ServiceManager");
    error_t ret = E_OK;

    mWatchDogThread = std::make_shared<android::HandlerThread>("WatchDogThread");
    mWatchDogThread->start();
    mWatchDogHandler = std::make_shared<WatchDogHandler>(mWatchDogThread->getLooper(), *this);
    mWatchDogHandler->sendEmptyMessage(kKickWatchDog);

    mServiceManagerAdaptor = std::make_shared<ServiceManagerAdaptor>();
    mServiceManagerAdaptor->RegisterServiceToBus();

    GMainLoop* loop = g_main_loop_new(NULL, false);
    g_main_loop_run(loop);
    g_main_loop_unref(loop);

    mServiceManagerAdaptor->UnregisterServiceFromBus();

    return ret;
}

error_t ServiceManager::onStop()
{
    return E_OK;
}

ServiceManager::WatchDogHandler::WatchDogHandler(const std::shared_ptr<android::Looper> &looper, ServiceManager &service)
    : android::Handler(looper), mService(service)
{
}

ServiceManager::WatchDogHandler::~WatchDogHandler()
{   
}

void ServiceManager::WatchDogHandler::handleMessage(const std::shared_ptr<android::Message> &msg)
{
    if(msg->what == mService.kKickWatchDog)
    {
        LOG_INFO("CPU usage: %s", android::Commander::execute("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage \"%\"}'").c_str());
        LOG_INFO("RAM usage: %s", android::Commander::execute("free | grep Mem | awk '{usage = $3/$2 * 100.0 } END { print usage \"%\"}'").c_str());
        sendEmptyMessageDelayed(mService.kKickWatchDog, mService.kWatchDogInterval);
    }
    else
    {
        LOG_ERROR("Invalid Message [%d]", msg->what);
    }
}